'X-Powerd-By' is a common non-standard HTTP response header. This source is optional and can be manipulated by the server, making it a non-trusty source.
'HEAD HTTP' is extra information about the request / response (where it comes from, what status it has, ids, ips etc.). This source is not optional and cannot be manipulated by the server, making it a trusty source.
