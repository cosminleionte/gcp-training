Transport Layer Security aka TLS is a widely adopted security protocol designed to facilitate privacy and data security for communications over the Internet.
TLS is also the older brother of SSL (Secure Socket Layers).
With TLS it is also desirable that a client connecting to a server is able to validate ownership of the server’s public key. 
This is normally undertaken using an X.509 digital certificate issued by a trusted third party known as a Certificate Authority (CA) which asserts the authenticity of the public key.
