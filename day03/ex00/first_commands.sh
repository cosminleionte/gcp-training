#!/bin/bash

gcloud auth list

echo "[core]"
echo "project = "$(gcloud config list --format 'value(core.project)' 2>/dev/null)
printf "\n"

gcloud config get-value account
