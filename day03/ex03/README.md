The internal IP address exists inside every electronic that is connected to the router. It's only purpose is to help the route differentiate the devices that are connected to it: the PC, cell phone etc.
The external IP address is the address that the router connects you to the internet. This is what websites see for example.
