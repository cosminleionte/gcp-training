from flask import Flask, render_template

app = Flask(__name__, template_folder="app/templates/public", static_folder='app/static')

@app.route('/hello_to_training')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run()
