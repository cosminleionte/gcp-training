HTTP is a protocol for fetching resources such as HTML documents. 
It is the foundation of any data exchange on the Web and it is a client-server protocol, which means requests are initiated by the recipient, usually the Web browser.
A "requirements.txt" file is a log file that contains all of the libraries that need to be downloaded for a project to run.