from flask import Flask, render_template

app = Flask(__name__, template_folder="app/template/public", static_folder='app/static')

@app.route('/hello_to_training/<name>')
def index(name):
    my_html = '''
        <!DOCTYPE HTML>
        <head>
            <link type="text/css" rel = "stylesheet" href = "{{ url_for('static',filename='css/style.css') }}"/>
        </head>
        <body>
            <h1 id="title">Hy {}!</h1>
        </body>
    '''.format(name)
    f = open("app/template/public/index.html", "w")
    f.write(my_html)
    f.close()
    return render_template('index.html')

if __name__ == '__main__':
    app.run(port=8080)
