def create_types():
	with open('demo.txt', 'r') as file:
		data = file.read().replace('\n', '')
	if not data:
		raise EOFError("Invalid input")
	data_split = data.split(',')
	for nb in data_split:
		if not nb.isnumeric():
			raise TypeError("Invalid input")
	for i in range(len(data_split)):
		data_split[i] = int(data_split[i])
	data_tuple = tuple(data_split)
	data_dict = {}
	i = 0
	for nb in data_split:
		data_dict[i] = nb
		i = i + 1
	return data_split, data_tuple, data_dict
