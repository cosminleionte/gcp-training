List vs Tuple vs Dictionaries
A list is a collection of ordered data. A tuple is an ordered collection of data. A dictionary is an unordered collection of data that stores data in the format key:value.
As a representation, list are declared with [], tuples with (), and dictionaries with {}.
For a list, we can add an element using the 'append()' method, which will put an element at the end. Tuples are immutable, which means that no element can be added or removed.
Dictionaries use 'update()' method for adding and 'pop()' for removing an element. Here it is required to know both the 'key' and the 'value'.
