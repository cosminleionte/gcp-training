def count_words():
	try:
		str = input()
		if not str:
			raise EOFError("No input provided")
	except EOFError:
		raise
	str_list = str.split()
	counter = len(str_list)
	return counter
