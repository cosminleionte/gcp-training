A stack trace is a report of the active stack frames at a certain point in time during the execution of a program. 
When a program is running, memory is dynamically allocated in two places: the stack and the heap. 
Memory is continuously allocated on a stack but not on a heap, thus reflective of their names. Stack also refers to a programming construct, thus to differentiate it, this stack is referred to as the program's function call stack.
A stack trace allows tracking the sequence of nested functions.
