def get_details():
	import platform
	import datetime
	e = datetime.datetime.now()
	print ("Python " + platform.python_version())
	if e.month > 9:
		print ("Current date and time: " + "%s-%s-%s" % (e.day, e.month, e.year) + " " + "%s:%s:%s" % (e.hour, e.minute, e.second))
	else:
		print ("Current date and time: " + "%s-0%s-%s" % (e.day, e.month, e.year) + " " + "%s:%s:%s" % (e.hour, e.minute, e.second))
