def main():
	stri = input()
	if not stri:
		raise EOFError("No input provided")
	str_split = stri.split()
	for nb in str_split:
		if not nb.isnumeric():
			raise TypeError("Incorrect type")
	if len(str_split) > 2:
		raise ValueError("invalid interval")
	nb1 = int(str_split[0])
	nb2 = int(str_split[1])
	if nb2 < nb1:
		raise ValueError("invalid interval")
	arr = ""
	while (nb1 <= nb2):
		aux = str(nb1)
		arr = arr + aux + " "
		nb1 = nb1 + 1
	print (arr)

main()
