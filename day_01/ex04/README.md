Errors vs Exceptions.
Errors are usually based on the syntax. They appear when the syntax of the cod is written wrong or it is missing.
Exceptions appear while the program is running. These can occur from a wide variety: division by zero, wrong data input, no input etc.
Error cannot be managed since the program will simply just not run, however the Exceptions can be managed. We can either throw the standard error or to handle the program in such a way that it will not stop (usually the best alternative for friendly user).
