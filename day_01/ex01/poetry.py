def main():
	print ("Again, I rely only on words")
	print ("\tThere is no music to break out of anyone's bone")
	print ("\t\tNor does the soul have in itself the peace proper to the happy hours")
	print ("\tNor the strong friend close to sad and great ideas.")
	print ("    And neither is an omniscient old fox")

main()
