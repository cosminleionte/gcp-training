def gen_tuples(start, end):
	list = [(x, y) for y in range(start, end + 1) for x in range(y, 0, -1)]
	return list
