List comprehension is a technique that allows to write a shorter syntax when it is needed to create a new list of values from another list.
This technique is similar to java8 streams and can reduce up to 6 lines of code.
