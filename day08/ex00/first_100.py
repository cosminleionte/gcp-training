from google.cloud import bigquery

client = bigquery.Client()


query = """
    SELECT a.id, title, score
    FROM `bigquery-public-data.stackoverflow.posts_questions` a
    JOIN (
        SELECT CAST(REGEXP_EXTRACT(content, r'stackoverflow.com/questions/([0-9]+)/') AS INT64) id, COUNT(*) c, MIN(sample_path) sample_path
        FROM `fh-bigquery.github_extracts.contents_js`
    WHERE content LIKE '%stackoverflow.com/questions/%'
    GROUP BY 1
    HAVING id>0
    ORDER BY 2 DESC
    LIMIT 100
    ) b
    ON a.id=b.id
"""
query_job = client.query(query)  # Make an API request.

print("The query data:")
for row in query_job:
    # Row values can be accessed by field name or index.
    print(row)